import React, { useEffect } from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
const Splash = ({navigation}) => {
    useEffect(()=>{
        setTimeout(()=>{
            navigation.replace("Login")
        }, 3000)
    }, [])

    return (
        <View style={styles.container}>
            <View style={styles.content}>
            <Text style={{fontWeight: 'bold', fontSize: 30,}}>SPLASH SCREEN</Text>
            <Image source={require('./images/logo.png')} />

            </View>
           
        </View>
    )
}

export default Splash

const styles = StyleSheet.create( {
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        padding: 16
    },
    content: {
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center'
    }
});