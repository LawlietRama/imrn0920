import React, { useState } from 'react'

import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { NavigationContainer } from '@react-navigation/native';

import Home from './Home';
import Login from './Login';
import Splash from './Splash';
import Detail from './Detail'
import Profile from './Profile'
import { UserContext } from './userContext';

const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();

const index = () => {
    const [value, setValue] = useState('Username')
    return (
        <NavigationContainer >
            <UserContext.Provider value={{ value, setValue }}>
                <Stack.Navigator initialRouteName={Login}>
                    <Stack.Screen name="Splash" component={Splash} />
                    <Stack.Screen name="Login" component={Login} />
                    <Stack.Screen name="MainApp" component={MainApp} options={{ title: 'Detective Movies Collection', headerLeft: null }}/>
                </Stack.Navigator>
            </UserContext.Provider>
        </NavigationContainer>
    )
}

const HomeStack = createStackNavigator()
const HomeStackScreen = () => {
    return (
        <HomeStack.Navigator>
            <HomeStack.Screen name="Home" component={Home}/>
            <HomeStack.Screen name="Detail" component={Detail}/>
        </HomeStack.Navigator>
    )
}

const MainApp = () => {
    return(
        <Tab.Navigator>
            <Tab.Screen name="Home" component={HomeStackScreen} />
            <Tab.Screen name="Profile" component={Profile}/>
        </Tab.Navigator>
    )
}

export default index