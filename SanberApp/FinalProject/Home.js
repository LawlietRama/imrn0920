import React, { useContext } from 'react'
import { StyleSheet, Text, View, Image, FlatList, TouchableOpacity } from 'react-native'
import { UserContext } from './userContext';

// import MovieCard from './MovieCard'

const Home = ({navigation}) => {
    const {value, setValue } = useContext(UserContext)
    console.log(value)

    var movie = {
        title: 'Ace Ventura: Pet Detective',
        image: 'https://m.media-amazon.com/images/M/MV5BYmVhNmFmOGYtZjgwNi00ZGQ0LThiMmQtOGZjMDUzNzJhMGIzXkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_SX300.jpg',
        id: 'tt0109040',
    }
    var movie2 = {
        title: 'Pokémon Detective Pikachu',
        image: 'https://m.media-amazon.com/images/M/MV5BNDU4Mzc3NzE5NV5BMl5BanBnXkFtZTgwMzE1NzI1NzM@._V1_SX300.jpg',
        id: 'tt5884052',
    }
    var movie3 = {
        title: 'The Great Mouse Detective',
        image: 'https://m.media-amazon.com/images/M/MV5BMDUzMTliZDYtMmRjZS00Zjk3LWIwZjEtZjc4N2NjYjdmY2FjXkEyXkFqcGdeQXVyMjA0MDQ0Mjc@._V1_SX300.jpg',
        id: 'tt0091149',
    }
    var movie4 = {
        title: 'Detective Byomkesh Bakshy!',
        image: 'https://m.media-amazon.com/images/M/MV5BMjE2MDgyOTgzN15BMl5BanBnXkFtZTgwNzY2NTYyNTM@._V1_SX300.jpg',
        id: 'tt3447364',
    }
    var movie5 = {
        title: 'Detective Dee: Mystery of the Phantom Flame',
        image: 'https://m.media-amazon.com/images/M/MV5BZTRiYzIwMzYtYmMyYi00NTFiLTgzZDgtMjllZDUwOTNhMWY3L2ltYWdlXkEyXkFqcGdeQXVyNTAyODkwOQ@@._V1_SX300.jpg',
        id: 'tt1123373',
    }
    var movie6 = {
        title: 'Detective Jagga',
        image: 'https://m.media-amazon.com/images/M/MV5BMTkyMzc1MTQxNl5BMl5BanBnXkFtZTgwMDIzNzM3MjI@._V1_SX300.jpg',
        id: 'tt4129428',
    }
    var movie7 = {
        title: 'The Singing Detective',
        image: 'https://m.media-amazon.com/images/M/MV5BMjE0NTExOTA2Ml5BMl5BanBnXkFtZTcwOTIyNTAwMQ@@._V1_SX300.jpg',
        id: 'tt0314676',
    }
    var movie8 = {
        title: 'Detective Story',
        image: 'https://m.media-amazon.com/images/M/MV5BMTkxYzQ0MzgtMzUyOS00OGYyLTg0MDAtODY5MTM1NmQwMmZmXkEyXkFqcGdeQXVyMDI2NDg0NQ@@._V1_SX300.jpg',
        id: 'tt0043465',
    }
    
    let movieArr = []
    movieArr.push(movie)
    movieArr.push(movie2)
    movieArr.push(movie3)
    movieArr.push(movie4)
    movieArr.push(movie5)
    movieArr.push(movie6)
    movieArr.push(movie7)
    movieArr.push(movie8)
    

    const _movieCard = ({item, index}) => {
        return (
            <View style={styles.MovieCardBox}>
                <TouchableOpacity style={styles.Button} onPress={() => navigation.push('Detail', { movieId: item.id})}>
                <Image style={styles.MovieImage} source={{uri: item.image}}/>
                </TouchableOpacity>
            </View>
        )
    }

    return (
        <FlatList
            data={movieArr}
            numColumns={2}
            renderItem={this, _movieCard}
            keyExtractor = { (item, index) => index.toString()}
        />
    )
}

export default Home

const styles = StyleSheet.create({
    MovieImage: {
        width: '100%',
        height: '100%',
    },
    MovieCardBox: {
        marginTop: 4,
        marginLeft: 15,
        width: '43%',
        height: 220,
        borderRadius: 10,
        marginBottom: 5,
    },
    Button: {
        height: '100%',
        width: '100%',
    },
});