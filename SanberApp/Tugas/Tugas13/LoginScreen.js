import React from 'react';
import {View, Text, StyleSheet, Platform, Image, TouchableOpacity, FlatList, TextInput} from 'react-native';

const Card =({title})=>{
    return(
      <TouchableOpacity style={styles.Button2}>
        <Text>{title}</Text>
      </TouchableOpacity>
    )
  }

export default class App extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.content}>
                    <Image source={require('./assets/logo.png')} />
                </View>
                <View style={styles.content}>
                    <Text style={styles.title}>Login</Text>
                </View>
                <View style={styles.content}>
                    <View style={styles.textbox, {marginBottom:50}}>
                        <Text style={styles.textboxtitle}>Username/Email</Text>
                        <Card/> 
                        <Text style={styles.textboxtitle}>Password</Text>
                        <Card/> 
                    </View>
                
                </View>
                <View style={styles.content}>
                    <View style={styles.button1}>
                        <Text style={styles.buttontext}>Masuk</Text>
                    </View>
                </View>
                <View style={styles.content}>
                        <Text style={styles.buttontext, {color: '#3EC6FF', padding: 22}}>atau</Text>
                </View>
                <View style={styles.content}>
                    <View style={styles.button3}>
                        <Text style={styles.buttontext}>Daftar</Text>
                    </View>
                </View>
            </View>
        )
    }
};

const styles = StyleSheet.create( {
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        padding: 16
    },
    content: {
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center'
    },
    title: {
        alignItems: 'center',
        color: '#003366',
        fontSize: 35,
        fontWeight: 'bold',
        paddingTop: 50
    },
    textbox: {
        flexDirection: 'column'
    },
    textboxtitle: {
        color: '#003366',
        paddingTop: 10
    },
    button1: {
        width: 140,
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor:'#3EC6FF',
        borderRadius: 16
    },
    button3: {
        backgroundColor: '#003366',
        width: 140,
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 16
    },
    buttontext: {
        fontSize: 24,
        color: 'white'
    },
    Button2:{
        width: 300,
        height: 50,
        backgroundColor:'white',
        borderWidth: 1,
        borderColor: '#003366',
        justifyContent:'center',
        alignItems:'center'
      }
});