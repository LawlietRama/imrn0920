import React from 'react';
import {View, Text, StyleSheet, Platform, Image, TouchableOpacity, FlatList, TextInput} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

export default class App extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.titlebar}>
                    <Text style={styles.titletext}>Tentang Saya</Text>
                    <View style={styles.background1}>
                        <Icon style={{color: '#CACACA'}} name="md-person" size={170}/>
                    </View>
                    <Text style={styles.titlename}>M. Aulia Ramadhan</Text>
                    <Text style={{color: '#3EC6FF', padding: 8}}>React Native Developer</Text>                 
                </View>
                
                <View style={styles.detailbar}>
                    <View style={styles.background2}>
                        <View style={styles.detailbar1}>
                            <Text style={styles.detailtitle}>Portofolio</Text>
                            <View style={styles.detailporto}>
                                <View style={styles.detailbar}>
                                    <Icon style={{color: '#3EC6FF'}} name="logo-github" size={40}/>
                                    <Text style={{color: '#003366', fontSize: 16, fontWeight: 'bold'}}>@LawlietRama</Text>
                                </View>
                                <View style={styles.detailbar}>
                                    <Icon style={{color: '#3EC6FF'}} name="logo-steam" size={40}/>
                                    <Text style={{color: '#003366', fontSize: 16, fontWeight: 'bold'}}>LawlietRama</Text>
                                </View>
                            </View>
                                
                        </View>
                    </View>
                </View>

                <View style={styles.detailbar}>
                    <View style={styles.background2}>
                        <View style={styles.detailbar1}>
                            <Text style={styles.detailtitle}>Hubungi Saya</Text>
                            <View style={styles.detailbar}>
                                <View style={styles.detailporto}>
                                    <Icon style={{color: '#3EC6FF', paddingRight: 12}} name="logo-instagram" size={40}/>
                                    <Text style={{color: '#003366', fontSize: 16, fontWeight: 'bold'}}>@LawlietRama</Text>
                                </View>
                                <View style={styles.detailporto}>
                                    <Icon style={{color: '#3EC6FF', paddingRight: 12}} name="logo-steam" size={40}/>
                                    <Text style={{color: '#003366', fontSize: 16, fontWeight: 'bold'}}>LawlietRama</Text>
                                </View>
                            </View>
                                
                        </View>
                    </View>
                </View>
                    
                    
            </View>
        )
    }
};

const styles = StyleSheet.create( {
    container: {
        flex: 1,
        justifyContent: 'center'
    },
    titlebar: {
        flexDirection: 'column',
        padding: 12,
        alignItems: 'center'
    },
    background1: {
        width: 200,
        height: 200,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor:'#EFEFEF',
        borderRadius: 100
    },
    titletext: {
        fontSize: 36, 
        fontWeight: "bold",
        color: '#003366',
        padding: 16
    },
    titlename: {
        fontSize: 24,
        fontWeight: "bold",
        color: '#003366',
        paddingTop: 24
    },
    detailbar: {
        flexDirection: 'column',
        padding: 12,
        alignItems: 'center',
        justifyContent: 'space-evenly'
    },
    background2: {
        width: 325,
        height: 140,
        justifyContent: 'center',
        backgroundColor:'#EFEFEF',
    },
    detailbar1: {
        flex: 1,
        padding: 8
    },
    detailtitle: {
        fontSize: 18,
        color: '#003366',
        borderBottomWidth: 1
    },
    detailporto: {
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        alignItems: 'center',
        padding: 12
    }

});