import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createDrawerNavigator } from "@react-navigation/drawer";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";

import { MaterialCommunityIcons } from "@expo/vector-icons";
import LoginScreen from "./LoginScreen";
import AboutScreen from "./AboutScreen";
import ProjectScreen from "./ProjectScreen";
import AddScreen from "./AddScreen";
import SkillScreen from "./SkillScreen";

const Tab = createBottomTabNavigator();
const Drawer = createDrawerNavigator();
const Stack = createStackNavigator();

const MainApp = () => {
  return (
    <Tab.Navigator>
      <Tab.Screen name="Skill" component={SkillScreen} />
      <Tab.Screen name="Project" component={ProjectScreen} />
      <Tab.Screen name="Add" component={AddScreen} />
    </Tab.Navigator>
  );
};
const MyDrawer = () => {
  return (
    <Drawer.Navigator
      initialRouteName="About"
      style={{
        alignItems: "center",
        justifyContent: "center",
        fontSize: 24,
        paddingBottom: 10,
      }}
      tabBarOptions={{
        activeTintColor: "red",
        inactiveTintColor: "gray",
      }}
    >
      <Drawer.Screen name="About" component={AboutScreen} />
      <Drawer.Screen name="MyTabs" component={MainApp} />
    </Drawer.Navigator>
  );
};
const index = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName={LoginScreen}>
        <Stack.Screen name="Login" component={LoginScreen} />
        <Stack.Screen name="About" component={AboutScreen} />
        <Stack.Screen name="Project" component={ProjectScreen} />
        <Stack.Screen name="Add" component={AddScreen} />
        <Stack.Screen name="Skill" component={SkillScreen} />
        <Stack.Screen name="MainApp" component={MainApp} />
        <Stack.Screen name="MyDrawer" component={MyDrawer} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default index;

const styles = StyleSheet.create({});
