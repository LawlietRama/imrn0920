import React from "react";
import { StyleSheet, Text, View } from "react-native";

const AddScreen = () => {
  return (
    <View style={styles.container}>
      <Text style={styles.textt}>Halaman Tambah</Text>
    </View>
  );
};

export default AddScreen;

const styles = StyleSheet.create({
  container: {
    justifyContent: "center",
    alignItems: "center",
  },
  textt: {
    fontSize: 32,
    fontWeight: "bold",
    color: "blue",
  },
});
