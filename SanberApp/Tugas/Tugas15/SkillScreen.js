import React, { Component } from "react";
import { Text, View, StyleSheet, Image, FlatList } from "react-native";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import data from "./skillData.json";
import SkillItem from "./components/SkillItem";
export default class SkillScreen extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.topBar}>
          <Image
            style={styles.logo}
            source={require("../Tugas15/images/logo.png")}
          />
        </View>
        <View style={styles.account}>
          <MaterialCommunityIcons
            name="account-circle"
            size={32}
            color="#3EC6FF"
          />
          <View style={styles.accountName}>
            <View style={styles.itemAcount}>
              <Text style={styles.accountHi}>Hai,</Text>
              <Text style={styles.accountTitle}>Asmuni Haris</Text>
            </View>
          </View>
        </View>
        <Text style={styles.judul}>SKILL</Text>
        <View style={styles.garis} />
        <View style={styles.cardCategory}>
          <View style={styles.itemCategory}>
            <Text style={styles.txtCategory}>Library/Framework</Text>
          </View>
          <View style={styles.itemCategory}>
            <Text style={styles.txtCategory}>Bahasa Pemrograman</Text>
          </View>
          <View style={styles.itemCategory}>
            <Text style={styles.txtCategory}>Teknologi</Text>
          </View>
        </View>
        <View style={styles.body}>
          {/* <SkillItem skill={data.items[0]} /> */}
          <FlatList
            data={data.items}
            renderItem={(skill) => <SkillItem skill={skill.item} />}
            keyExtractor={(item) => item.id.toString()}
            ItemSeparatorComponent={() => (
              <View style={{ height: 0.5, backgroundColor: "#E5E5E5" }} />
            )}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    margin: 16,
  },
  topBar: {
    marginTop: 15,
    flexDirection: "row-reverse",
  },
  logo: {
    height: 51,
    width: 187.5,
  },
  account: {
    marginTop: 15,
    flexDirection: "row",
    alignItems: "center",
  },
  accountName: {
    flexDirection: "column",
    justifyContent: "center",
    marginLeft: 10,
  },
  accountItem: {
    marginLeft: 25,
  },
  accountHi: {
    fontSize: 12,
    height: 14,
    color: "#000000",
  },
  accountTitle: {
    fontSize: 16,
    color: "#3EC6FF",
    height: 19,
    color: "#003366",
  },

  judul: {
    fontSize: 36,
    color: "#003366",
    fontWeight: "bold",
    height: 42,
    marginBottom: 5,
  },
  garis: {
    borderBottomColor: "#3EC6FF",
    borderBottomWidth: 5,
  },
  cardCategory: {
    marginTop: 5,
    flexDirection: "row",
    justifyContent: "space-between",
  },
  itemCategory: {
    backgroundColor: "#B4E9FF",
    justifyContent: "center",
    padding: 5,
    borderRadius: 10,
  },
  txtCategory: {
    color: "#003366",
    fontWeight: "bold",
    fontSize: 12,
    height: 14,
  },
  body: {
    flex: 1,
  },
});
