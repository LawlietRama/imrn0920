import React from "react";
import { StyleSheet, Text, View } from "react-native";

const ProjectScreen = () => {
  return (
    <View style={styles.container}>
      <Text style={styles.textt}>Halaman Project</Text>
    </View>
  );
};

export default ProjectScreen;

const styles = StyleSheet.create({
  container: {
    justifyContent: "center",
    alignItems: "center",
  },
  textt: {
    fontSize: 32,
    fontWeight: "bold",
    color: "red",
  },
});
