import React, { Component } from "react";
import { Text, StyleSheet, View } from "react-native";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { Ionicons } from "@expo/vector-icons";
export default class SkillItem extends Component {
  render() {
    let skill = this.props.skill;
    return (
      <View style={styles.container}>
        <View style={styles.cardSkill}>
          <MaterialCommunityIcons
            name={skill.iconName}
            size={86}
            color="#003366"
            style={{ paddingLeft: 13 }}
          />
          <View style={styles.skillDetil}>
            <Text style={styles.namaSkill}>{skill.skillName}</Text>
            <Text style={styles.namaKategory}>{skill.categoryName}</Text>
            <Text style={styles.persen}>{skill.percentageProgress}</Text>
          </View>
          <Ionicons
            name="ios-arrow-forward"
            size={72}
            color="#003366"
            style={{ paddingRight: 13 }}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  cardSkill: {
    marginTop: 10,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    backgroundColor: "#B4E9FF",
    borderRadius: 10,
  },
  skillDetil: {
    flexDirection: "column",
    width: "50%",
  },
  namaSkill: {
    fontSize: 24,
    fontWeight: "bold",
    color: "#003366",
    height: 28,
  },
  namaKategory: {
    fontSize: 16,
    fontWeight: "bold",
    color: "#3EC6FF",
    height: 18.75,
  },
  persen: {
    alignSelf: "flex-end",
    fontSize: 48,
    fontWeight: "bold",
    color: "#FFFFFF",
    height: 56,
  },
});
