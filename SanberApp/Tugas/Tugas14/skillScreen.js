import React from 'react';
import { View, Text, Image, ScrollView, FlatList, TouchableOpacity, StyleSheet } from 'react-native';

import data from './skillData.json';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { createPortal } from 'react-dom';


export default class App extends React.Component {
    render() {
        return (
            <View style={styles.container}>

                <View style={{alignItems: 'flex-end'}}>
                    <Image source={require('./assets/logo.png')} style={{width:187.5, height: 51}} />
                </View>

                <View style={{flexDirection: 'row'}}>
                    <View>
                        <TouchableOpacity>
                            <Icon style={{color: '#B4E9FF', paddingLeft: 16}} name="account-circle" size={30}/>
                        </TouchableOpacity>
                        

                    </View>
                    <View style={{paddingLeft: 11}}>
                            <Text>Hei,</Text>
                            <Text style={styles.titlename}>M. Aulia Ramadhan</Text>
                    </View>
                </View>

                <View style={{padding: 16}}>
                    <Text style={styles.titletext}>SKILL</Text>
                </View>

                <View style={{flexDirection: 'row', marginLeft: 16, marginRight:16, justifyContent: 'space-evenly'}}>
                    <View style={{backgroundColor:'#B4E9FF', padding: 8, borderRadius: 8}}>
                        <Text style={{color: '#003366', fontWeight: 'bold', fontSize: 11}}>Library / Framework</Text>
                    </View>
                    <View style={{backgroundColor:'#B4E9FF', padding: 8, borderRadius: 8}}>
                        <Text style={{color: '#003366', fontWeight: 'bold', fontSize: 11}}>Bahasa Pemrograman</Text>
                    </View>
                    <View style={{backgroundColor:'#B4E9FF', padding: 8, borderRadius: 8}}>
                        <Text style={{color: '#003366', fontWeight: 'bold', fontSize: 11}}>Teknologi</Text>
                    </View>
                </View>

                <View>
                    <FlatList
                        data={data.items}
                        renderItem={({item})=>
                        <View style={{flexDirection: 'row', backgroundColor: '#B4E9FF', marginTop: 16, marginLeft: 16, marginRight: 16, justifyContent: 'space-between', padding: 8, alignItems: 'center'}}>
                            <Icon style={{color: '#003366'}} name={item.iconName} size={75}/>
                            <View style={{flexDirection: 'row'}}>
                                <View style={{flexDirection: 'column'}}>
                                    <Text>{item.skillName}</Text>
                                    <Text>{item.categoryName}</Text>
                                    <View style={{alignItems: 'flex-end'}}>
                                        <Text style={{color: 'white', fontSize: 40, fontWeight: 'bold'}}>{item.percentageProgress}</Text>
                                    </View>
                                    
                                </View>
                                
                                <View style={{alignItems: 'flex-end'}}>
                                    <Icon style={{color: '#003366', paddingLeft: 16}} name='chevron-right' size={75}/>
                                </View>
                                
                            </View>
                        
                        </View>

                        }
                        
                    />
                </View>
                
                
                    
                    
            </View>
        )

    }

}

const styles = StyleSheet.create( {
    container: {
        flex: 1
    },
    titlebar: {
        flexDirection: 'column',
        padding: 12,
        alignItems: 'center'
    },
    background1: {
        width: 200,
        height: 200,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor:'#EFEFEF',
        borderRadius: 100
    },
    titletext: {
        fontSize: 36,
        color: '#003366',
        borderBottomWidth: 3,
        borderBottomColor: '#B4E9FF'
    },
    titlename: {
        fontSize: 16,
        color: '#003366'
    },
    detailbar: {
        flexDirection: 'column',
        padding: 12,
        alignItems: 'center',
        justifyContent: 'space-evenly'
    },
    background2: {
        width: 325,
        height: 140,
        justifyContent: 'center',
        backgroundColor:'#EFEFEF',
    },
    detailbar1: {
        flex: 1,
        padding: 8
    },
    detailtitle: {
        fontSize: 18,
        color: '#003366',
        borderBottomWidth: 1
    },
    detailporto: {
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        alignItems: 'center',
        padding: 12
    }

});