import React from 'react';
import {
    Image, Platform, View, Text, ScrollView, TextInput, StyleSheet, Button,
    TouchableOpacity, KeyboardAvoidingView
} from 'react-native';

export default function App( {navigation} ) {
    return (

        <KeyboardAvoidingView
            behavior={Platform.OS == "ios" ? "padding" : "height"}
            style={styles.container}>
            <ScrollView>
            <View style={styles.container}>
       
        <Text style={{fontSize: 30, textShadowRadius: 23}}>Welcome</Text>
        <Text style={{fontSize: 12}}>Sign up to Continue</Text>
        
        <View style={{backgroundColor: 'white', maxWidth: 366, maxHeight: 536, shadowRadius: 10, margin: 32, padding: 13}}>
            <View style={styles.inputView}>
            <Text style={styles.inputViewTitle}>Name</Text>
            <TextInput style={styles.inputText} />
            </View>

            <View style={styles.inputView}>
            <Text style={styles.inputViewTitle}>Email</Text>
            <TextInput style={styles.inputText} />
            </View>

            <View style={styles.inputView}>
            <Text style={styles.inputViewTitle}>Phone Number</Text>
            <TextInput secureTextEntry style={styles.inputText} />
            </View>
            <View style={styles.inputView}>
            <Text style={styles.inputViewTitle}>Password</Text>
            <TextInput secureTextEntry style={styles.inputText} />
            </View>
            <View style={{alignItems: 'center'}}>
                <TouchableOpacity style={styles.daftarBtn}
                onPress={() => navigation.navigate('HomeScreen')} >
                <Text style={styles.loginText}>Sign Up</Text>
                </TouchableOpacity>

                <View style={{flexDirection: 'row', padding: 11}}>
                    <Text>Already have an account? </Text>

                    <TouchableOpacity
                        onPress={() => navigation.navigate('Login')} >
                        <Text style={{color: '#F77866', textShadowRadius: 12}}>Sign In</Text>
                    </TouchableOpacity>
                </View>
                
            </View>
            
        </View>
        
      </View>

            </ScrollView>
        </KeyboardAvoidingView>


    );
}


const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: "#fff",
      padding: 41,
      justifyContent: 'center'
      // margin: 0,
      // borderWidth: 1,
      // borderColor: "#003366",
      // borderRadius: 10,
      // justifyContent: "center",
    },
    logo: {
      marginTop: 24,
      // width: 98,
      // height: 22,
    },
    judul: {
      // marginTop: 64,
      marginBottom: 30,
      fontSize: 24,
      color: "#003366",
    },
    inputViewTitle: {
      fontSize: 16,
      alignItems: "flex-start"
    },
    inputView: {
      width: "100%",
      height: 48,
      marginBottom: 20,
      marginTop: 10,
      justifyContent: "center",
    },
    inputText: {
      height: 48,
      paddingStart: 10,
      borderColor: "#003366",
      borderWidth: 1,
    },
    loginText: {
      color: "white",
      fontSize: 24,
    },
    loginBtn: {
      width: 140,
      backgroundColor: "#3EC6FF",
      borderRadius: 20,
      alignItems: "center",
      justifyContent: "center",
      height: 40,
      marginTop: 10,
      // marginBottom: 10,
    },
    daftarBtn: {
      width: 318,
      backgroundColor: "#F77866",
      borderRadius: 6,
      alignItems: "center",
      justifyContent: "center",
      height: 50,
      marginTop: 20,
      // marginBottom: 10,
    },
    textAtau: {
      marginTop: 10,
      fontSize: 24,
      color: "#3EC6FF",
      // marginBottom: 30,
    },
    body: {
      flex: 1,
    },
  });
  