//1. SOAL CLASS SCORE
class Score {
    constructor(subject, points, email) {
        this.subject = subject;
        this.points = points;
        this.email = email;
    }

    average() {
        var avg = 0;
        if(typeof (this.points) === 'object') {
            for(var i=0;i<this.points.length;i++) {
                avg += this.points[i];
            }
            return avg/this.points.length;
        }
        else if (typeof (this.points) === 'number') {
            return this.points;
        }

        
        
    }
}

myScore = new Score("Bahasa", [45,23,23], "aaaaa");
console.log(myScore.average());

//2. SOAL Create Score
const data = [
    ["email", "quiz-1", "quiz-2", "quiz-3"],
    ["abduh@mail.com", 78, 89, 90],
    ["khairun@mail.com", 95, 85, 88],
    ["bondra@mail.com", 70, 75, 78],
    ["regi@mail.com", 91, 89, 93]
  ]

viewScores = (data, subject) => {
    var subjectIndex;
    var subjectString;
    var arrayOfObject = [];
    for(i=1;i<data[0].length;i++) {
        if(data[0][i] == subject) {
            subjectIndex = i;
            subjectString = subject;
            break;
        }
    }

    for(i=1;i<data.length;i++) {
        var object = {
            email: data[i][0],
            subject: subjectString,
            points: data[i][subjectIndex]
        }
        arrayOfObject.push(object);
    }

    console.log(arrayOfObject);
} 

viewScores(data, "quiz-1");
viewScores(data, "quiz-2");
viewScores(data, "quiz-3");

//3. SOAL Recap Score
const data2 = [
    ["abduh@mail.com", 78, 89, 90],
    ["khairun@mail.com", 95, 85, 88],
    ["bondra@mail.com", 70, 75, 78],
    ["regi@mail.com", 91, 89, 93]
  ]

function recapScores(data2) {
    var arrayOfObject2;
    //counting average every student
    for(i=0;i<data2.length;i++) {
        var avg = 0;
        for(j=1;j<data2[i].length;j++) {
            
            avg += data2[i][j];
        }
        avg = avg/(data2[i].length-1);
        
        console.log(i+1 + ". Email: " + data2[i][0] + "\nRata-rata: " + avg);

        if(avg>70) {
            console.log("Predikat: participant");
        }
        else if(avg>80) {
            console.log("Predikat: graduate");
        }
        else if(avg>90) {
            console.log("Predikat: honour");
        }
    }
  }
  
  recapScores(data2);