//No. 1 Looping While
var x = 2;
console.log("LOOPING PERTAMA");
while(x <= 20) {
    console.log(x + " - I love coding");
    x+=2;
}

console.log("LOOPING KEDUA");
while(x > 2) {
    x-=2;
    console.log(x + " - I love coding");
}

console.log("\n");
//No. 2 Looping menggunakan for
for(i=1; i<=20; i++) {
    if(i % 3 == 0 && i % 2 != 0) {
        console.log(i + " - I Love Coding");
    }
    else if(i % 2 == 0) {
        console.log(i + " - Berkualitas");
    }
    else if(i % 2 != 0) {
        console.log(i + " - Santai");
    }
  }

console.log("\n");
//No. 3 Membuat Persegi Panjang #
for(i=0; i<4; i++) {
    for(j=0; j<8; j++) {
        process.stdout.write('#');
    }
    console.log('\n');
}

//No. 4 Membuat Tangga
for(i=0;i<=7;i++) {
    for(j=0; j<i; j++) {
        process.stdout.write('#');
    }
    console.log('\n');
}

console.log('\n');
//No. 5 Membuat Papan Catur
for(i=0;i<8;i++) {
    for(j=0;j<8;j++) {
        if((i+1) % 2 == 1) {
            if((j+1) % 2 == 1) {
                process.stdout.write(' ');
            }
            else {
                process.stdout.write('#');
            }
        }
        else {
            if((j+1) % 2 == 1) {
                process.stdout.write('#');
            }
            else {
                process.stdout.write(' ');
            }
        }
        
    }
    console.log('\n');
}